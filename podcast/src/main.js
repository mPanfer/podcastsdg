import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import PodcastExtract from './components/PodcastExtract';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import Vue2Filters from 'vue2-filters'

Vue.config.productionTip = false
Vue.component("podcastExtract", PodcastExtract);
Vue.use(Buefy);
Vue.use(Vue2Filters)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

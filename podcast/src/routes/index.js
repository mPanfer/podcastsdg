import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import PodcastDetail from '@/components/PodcastDetail'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            component: Home,
            name: 'Home'
        },
    		{
    			path: '/podcast/:idPodcast',
    			component: PodcastDetail,
    			name: 'PodcastDetail'
    		}
    ],
    linkActiveClass: "active",
    mode: "history"
})
